# Parallax Effects

```CRD.Parallaz``` provides a way to quickly implement a parallax event using background images positions or elements cliped in an element..

### Example

Before your closing ```<body>``` tag add:

```html
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="function.js"></script>
<script type="text/javascript" src="utils.js"></script>
<script type="text/javascript" src="parallax.js"></script>
```

Some basic styles:

```css
#parallax {
	width: 100%;
	height: 450px;
	background-image: url(example-bg.jpg);
}
```

Anywhere in your ```html``` document:

```html
<div id="parallax"></div>
```

Then initialize the navigation bar:

```javascript
// Initializes the parallax effect
var fixed = new CRD.Parallax.Background('#parallax');
```

*A packed and merged source (including function.js, utils.js and parallax.js) is available to use.*

### Arguments

Both ```CRD.Parallax.Background``` and ```CRD.Parallax.Element``` accepts two arguments:

Argument | Default | Description
-------- | ------- | -----------
element : ```String``` or ```Element``` | --- | Element or selector to which the effect will be applied.
options : ```Object``` | See specific default options for each type of effect. | Options to overwrite default values.

### Settings

#### Background Parallax

Option | Type | Default | Description
------ | ---- | ------- | -----------
css | ```Object``` | ```{ 'position' : 'relative', 'background-position' : 'center', 'background-repeat' : 'no-repeat', 'background-size' : 'cover' }``` | Default ```css``` properties to apply to the container element.
min | ```Number``` | 0 | Minimum percentage amount for the background position.
max | ```Number``` | 100 | Maximum percentage amount for the background position.

#### Element Parallax

Option | Type | Default | Description
------ | ---- | ------- | -----------
css | ```Object``` | ```{ 'position' : 'relative', overflow' : 'hidden' }``` | Default ```css``` properties to apply to the container element (the one that will adopt the element to which the effect will be applied).
height | ```Number``` | 0.8 | Number (0 to 1) to calculate the scale of the container element (0.8 will be the 80% of the element's height).
min | ```Number``` | 0 | Minimum percentage amount for the cliped element position.
max | ```Number``` | 100 | Maximum percentage amount for the cliped element position.

### Methods

#### Background Parallax

Method | Arguents | Returns | Description
------ | -------- | ------- | -----------
constructor | element : ```String``` or ```Element```, options : ```Object``` | A reference to the current object or class: ```CRD.Parallax.Background```. | Initializes the instance for the element with the provided options.
setBackgroundPosition | scrolled : ```Number``` | Calculates and sets the background position according to the current scroll position.

#### Element Parallax

Method | Arguents | Returns | Description
------ | -------- | ------- | -----------
constructor | element : ```String``` or ```Element```, options : ```Object``` | A reference to the current object or class: ```CRD.Parallax.Background```. | Initializes the instance for the element with the provided options.
setElementPosition | scrolled : ```Number``` | Calculates and sets the element position according to the current scroll position.
update | --- | Updates the element dimensions (height) and applies ```setElementPosition``` to set element position.

### jQuery Helper

A jQuery initialization helper is available to use directly with a jQuery object selector. The available helper for the background parallax effect id ```backgroundParallax```; for the element parallax, use  ```elementParallax```.

#### Example

Initialization thru the jQuery helper:

```javascript
// Initializes the background parallax effect
var parallax = jQuery('#parallax').backgroundParallax();

// Gets the instance for the same object
parallax === jQuery('#parallax').backgroundParallax('get'); // true
```

#### Arguments

Both jQuery helpers accepts several arguments, as described below:

Arguments | Example usage | Description
--------- | ------------- | -----------
options : ```Object``` | ```jQuery('.selector).backgroundParallax({ min : 50 })``` | Providing a single argument as an ```Object``` will initialize the correspondign  ```CRD.Parallax``` class (for element or background) using the passed object as initialization options.
getter : ```String``` | ```jQuery('.selector).backgroundParallax('get')``` | Providing the ```'get'``` argument, will get the instance of ```CRD.Parallax``` class (for element or background) for the selected element.
method : ```String```, ... ```*``` | ```jQuery('.selector).elementParallax('update')``` | Providing a valid method name (as a ```String```) an its arguments, will apply the method to the instance of ```CRD.Parallax``` class (for element or background) stored for the selected element.

### Dependencies

- CRD.Utils
- jQuery [http://www.jquery.com](jQuery)

### License

Copyright (c) 2016 Luciano Giordano

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details